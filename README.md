# README #

## MAVEN ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/maven.git

### Crear imagen
* docker build -t settingejemplo/maven .

### Crear contenedor ###
* docker run --rm --name my-maven -v "$PWD":/usr/src/mymaven:rw -w /usr/src/mymaven settingejemplo/maven mvn clean install wildfly:deploy
